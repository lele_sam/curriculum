

let navbarScroll = document.querySelector('#navbarScroll')

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            navbarScroll.classList.add('scrolled')
        }else {
            navbarScroll.classList.remove('scrolled')
        }
    }
    )
}

let logo = document.querySelector('#logo')
if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            logo.classList.remove('d-none')
        }else {
            logo.classList.add('d-none')
        }
    }
    )
}

let logobianco = document.querySelector('#logobianco')
if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            logobianco.classList.add('d-none')
        }else {
            logobianco.classList.remove('d-none')
        }
    }
    )
}


let navbarlink = document.querySelector('#melink')


if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            navbarlink.classList.add('scrolled')
        }else {
            navbarlink.classList.remove('scrolled')
        }
    }
    )
}

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50 && window.pageYOffset <= 1200) {
            navbarlink.classList.add('active')
        }else {
            navbarlink.classList.remove('active')
        }
    }
    )
}


let navbarprogrammilink = document.querySelector('#programmilink')

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            navbarprogrammilink.classList.add('scrolled')
        }else {
            navbarprogrammilink.classList.remove('scrolled')
        }
    }
    )
}

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 1200 && window.pageYOffset <= 2500) {
            navbarprogrammilink.classList.add('active')
        }else {
            navbarprogrammilink.classList.remove('active')
        }
    }
    )
}



let navbarprojlink = document.querySelector('#projlink')

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            navbarprojlink.classList.add('scrolled')
        }else {
            navbarprojlink.classList.remove('scrolled')
        }
    }
    )
}

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 2500 && window.pageYOffset <= 3200) {
            navbarprojlink.classList.add('active')
        }else {
            navbarprojlink.classList.remove('active')
        }
    }
    )
}

let navbarworklink = document.querySelector('#worklink')

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            navbarworklink.classList.add('scrolled')
        }else {
            navbarworklink.classList.remove('scrolled')
        }
    }
    )
}

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 3200 && window.pageYOffset <= 3900) {
            navbarworklink.classList.add('active')
        }else {
            navbarworklink.classList.remove('active')
        }
    }
    )
}

let navbarcontactlink = document.querySelector('#contactlink')

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 50) {
            navbarcontactlink.classList.add('scrolled')
        }else {
            navbarcontactlink.classList.remove('scrolled')
        }
    }
    )
}

if (window.innerHeight > 576) {
    document.addEventListener('scroll', () => {
        if (window.pageYOffset > 3900 && window.pageYOffset <= 5000) {
            navbarcontactlink.classList.add('active')
        }else {
            navbarcontactlink.classList.remove('active')
        }
    }
    )
}

